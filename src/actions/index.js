export const setSurveys = (surveys) => {
  return {
    type: "SET_SURVEYS",
    payload: surveys,
  };
};

export const setUser = (user) => {
  return {
    type: "SET_USER",
    payload: user,
  };
};

export const setRequests = (requests) => {
  return {
    type: "SET_REQUESTS",
    payload: requests,
  };
};
