import React, { Component } from "react";
import NavBar from "./components/NavBar";
import SurveysList from "./components/SurveysList";
import RequestsList from "./components/RequestsList";
import { database, auth } from "./firebase";
import FixFooter from "./components/FixFooter";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <NavBar database={database} auth={auth} />
          <Switch>
            <Route path="/requests" exact>
              <RequestsList database={database} />
              <FixFooter />
            </Route>
            <Route
              path="/requests/"
              render={() => <Redirect to={{ pathname: "/requests" }} />}
            />
            <Route path="/" exact>
              <SurveysList database={database} />
              <FixFooter />
            </Route>
            <Route render={() => <Redirect to={{ pathname: "/" }} />} />
          </Switch>
        </div>
      </Router>
    );
  }
}
export default App;
