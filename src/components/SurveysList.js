import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import PropTypes from "prop-types";
import { connect } from "react-redux";

const styles = (theme) => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  marginRight: {
    marginRight: "100px",
  },
  root: {
    width: "95%",
    marginLeft: "20px",
  },
  fullWidth: {
    width: "100%",
  },
  panelContent: {
    backgroundColor: "#f1f1f1", //#f1f1f1 , #d6d6d6
  },
  wordWrap: {
    overflowWrap: "break-word",
    wordWrap: "break-word",
    hyphens: "auto",
    margin: "20px",
    textAlign: "justify",
    textJustify: "inter-word",
  },
  wordWrapNoMargin: {
    overflowWrap: "break-word",
    wordWrap: "break-word",
    hyphens: "auto",
  },
  paddingOuter: {
    paddingLeft: "66px",
    paddingRight: "66px",
    paddingTop: "35px",
  },
  paragraphContent: {
    textAlign: "justify",
    textJustify: "inter-word",
  },
});

class SurveysList extends Component {
  state = {
    surveySearchString: "",
  };

  increaseCounterHandler = (title) => {
    let id = title.replace(/[^0-9a-z]/gi, "").toLowerCase();
    let surveys = this.props.surveys;

    // get count and increase one
    let count = 0;
    for (let i = 0; i < surveys.length; i++) {
      if (surveys[i].title === title) {
        count = surveys[i].count + 1;
      }
    }

    // update database
    this.props.database
      .ref("surveys/" + id)
      .update({
        count: count,
      })
      .catch((error) => {
        console.log(error);
      });
  };

  onSurveySearchInputChange = (event) => {
    this.setState({ surveySearchString: event.target.value });
  };

  processTitle = (t) => {
    if (t.length > 60) {
      return t.slice(0, 60) + " ...";
    }
    return t;
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.paddingOuter}>
        <h4
          className={classes.root}
          style={{ textAlign: "justify", fontWeight: "normal" }}
        >
          This is a simple web application built to group all FCSIT Final Year
          Project surveys in one page (FOR UM COMPUTER SCIENCE STUDENTS TO FILL
          IN ONLY). The survey list is sorted by number of clicks on the Google
          Form URL to ensure that all forms have fair attentions. This is only
          intended as a channel to reach FCSIT students for surveys. If you
          would like to on board your surveys to this app, kindly send a request
          via the button at top right corner of this application (login is not
          needed). GitLab link is at the bottom of this application. Use at your
          own responsibilities.
        </h4>
        {this.props.surveys ? (
          <div className={classes.root}>
            <TextField
              style={{ padding: 24 }}
              id="searchInput"
              placeholder="Search for Surveys"
              margin="normal"
              onChange={this.onSurveySearchInputChange}
            />
            <div>
              {this.props.surveys.map(
                (currentSurvey) =>
                  (new RegExp(this.state.surveySearchString, "gi").test(
                    currentSurvey.title
                  ) ||
                    this.state.surveySearchString === "") && (
                    <ExpansionPanel key={currentSurvey.key}>
                      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={7}>
                            <Typography className={classes.wordWrapNoMargin}>
                              {this.processTitle(currentSurvey.title)}
                            </Typography>
                          </Grid>
                          <Grid item xs={12} sm={3}>
                            <Link
                              href={currentSurvey.link}
                              target="_blank"
                              onClick={() =>
                                this.increaseCounterHandler(currentSurvey.title)
                              }
                            >
                              Go to Google Form Link
                            </Link>
                          </Grid>
                          <Grid item xs={12} sm={2}>
                            <Typography className={classes.heading}>
                              {currentSurvey.count} clicks
                            </Typography>
                          </Grid>
                        </Grid>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails className={classes.panelContent}>
                        <Grid container>
                          <Grid
                            item
                            xs={12}
                            className={classes.wordWrap}
                            style={{ marginBottom: "0" }}
                          >
                            {currentSurvey.title}
                          </Grid>
                          <Grid
                            item
                            xs={12}
                            className={classes.wordWrap}
                            style={{ whiteSpace: "pre-line" }}
                          >
                            {currentSurvey.desc}
                          </Grid>
                        </Grid>
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  )
              )}
            </div>
          </div>
        ) : (
          "No surveys found"
        )}
      </div>
    );
  }
}

SurveysList.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStatesToProps = (states) => {
  return {
    surveys: states.surveys,
    user: states.user,
  };
};

export default connect(mapStatesToProps)(withStyles(styles)(SurveysList));
