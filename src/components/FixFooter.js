import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    top: "150px",
  },
  footer: {
    backgroundColor: "#52317a",
    textAlign: "center",
    width: "100%",
    display: "block",
    paddingLeft: "0",
    paddingRight: "0",
    paddingTop: "15px",
    paddingBottom: "15px",
  },
  title: {
    textAlign: "center",
    width: "100%",
    fontSize: "13px",
    fontFamily: "sans-serif",
  },
}));

const FixFooter = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.footer}>
          <Typography className={classes.title}>
            Built by Jia Xiong and Chun Wah 29 April 2020 <br />{" "}
            https://gitlab.com/wing8169/fyp-survey-list <br />
            Built in several hours, might have bugs
            <br />
            Contact me via chinxiongwei@yahoo.com.my
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default FixFooter;
