import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

const styles = (theme) => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  marginRight: {
    marginRight: "100px",
  },
  root: {
    width: "95%",
    marginLeft: "20px",
  },
  fullWidth: {
    width: "100%",
  },
  panelContent: {
    backgroundColor: "#f1f1f1", //#f1f1f1 , #d6d6d6
  },
  wordWrap: {
    overflowWrap: "break-word",
    wordWrap: "break-word",
    hyphens: "auto",
    margin: "20px",
    textAlign: "justify",
    textJustify: "inter-word",
  },
  wordWrapNoMargin: {
    overflowWrap: "break-word",
    wordWrap: "break-word",
    hyphens: "auto",
  },
  paddingOuter: {
    paddingLeft: "66px",
    paddingRight: "66px",
    paddingTop: "35px",
  },
  paragraphContent: {
    textAlign: "justify",
    textJustify: "inter-word",
  },
});

class RequestsList extends Component {
  state = {
    requestSearchString: "",
  };

  onRequestSearchInputChange = (event) => {
    this.setState({ requestSearchString: event.target.value });
  };

  createSurvey = (event, title, link, desc) => {
    event.preventDefault();

    // validation
    if (title === "" || link === "" || desc === "") {
      alert("invalid input.");
      return;
    }
    let id = title.replace(/[^0-9a-z]/gi, "").toLowerCase();

    // update database
    this.props.database.ref("surveys/" + id).set({
      title: title,
      link: link,
      desc: desc,
      count: 0,
    });

    this.props.database
      .ref("requests/" + id)
      .update({
        isCreated: true,
      })
      .catch((error) => {
        console.log(error);
      });
    alert("survey created!");
  };

  deleteRequest = (event, title) => {
    event.preventDefault();
    let id = title.replace(/[^0-9a-z]/gi, "").toLowerCase();
    //update database
    if (
      window.confirm(
        "Confirm delete the request of " +
          title +
          "?\n The action cannot be UNDO!"
      )
    ) {
      this.props.database.ref("requests/" + id).remove();
      alert("Deleted: " + title);
    } else {
      alert("Bye!");
    }
  };

  processTitle = (t) => {
    if (t.length > 60) {
      return t.slice(0, 60) + " ...";
    }
    return t;
  };

  render() {
    if (!this.props.user) {
      return <Redirect to="/" />;
    }
    const { classes } = this.props;
    return (
      <div className={classes.paddingOuter}>
        <h4
          className={classes.root}
          style={{ textAlign: "justify", fontWeight: "normal" }}
        >
          This is a simple web application built to group all FCSIT Final Year
          Project surveys in one page (FOR UM COMPUTER SCIENCE STUDENTS TO FILL
          IN ONLY). The survey list is sorted by number of clicks on the Google
          Form URL to ensure that all forms have fair attentions. This is only
          intended as a channel to reach FCSIT students for surveys. If you
          would like to on board your surveys to this app, kindly send a request
          via the button at top right corner of this application (login is not
          needed). GitLab link is at the bottom of this application. Use at your
          own responsibilities.
        </h4>

        {this.props.user !== null &&
          (this.props.requests ? (
            <div className={classes.root}>
              <TextField
                style={{ padding: 24 }}
                id="searchInput"
                placeholder="Search for Requests"
                margin="normal"
                onChange={this.onRequestSearchInputChange}
              />
              <div>
                {this.props.requests.map(
                  (currentRequest) =>
                    (new RegExp(this.state.requestSearchString, "gi").test(
                      currentRequest.title
                    ) ||
                      this.state.requestSearchString === "") && (
                      <ExpansionPanel key={currentRequest.key}>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                          <Grid container spacing={2}>
                            <Grid item xs={12} sm={4}>
                              <Typography className={classes.wordWrapNoMargin}>
                                {this.processTitle(currentRequest.title)}
                              </Typography>
                            </Grid>
                            <Grid item xs={12} sm={2}>
                              <Link href={currentRequest.link} target="_blank">
                                Google Form Link
                              </Link>
                            </Grid>
                            <Grid item zeroMinWidth xs={12} sm={3}>
                              <Typography noWrap className={classes.heading}>
                                {currentRequest.email}
                              </Typography>
                            </Grid>
                            <Grid item>
                              {currentRequest.isCreated ? (
                                <Button fullWidth variant="contained" disabled>
                                  Created
                                </Button>
                              ) : (
                                <Button
                                  variant="contained"
                                  color="primary"
                                  onClick={(event) => {
                                    this.createSurvey(
                                      event,
                                      currentRequest.title,
                                      currentRequest.link,
                                      currentRequest.desc
                                    );
                                  }}
                                >
                                  create
                                </Button>
                              )}
                            </Grid>
                            <Grid item>
                              <Button
                                variant="contained"
                                color="secondary"
                                onClick={(event) => {
                                  this.deleteRequest(
                                    event,
                                    currentRequest.title
                                  );
                                }}
                              >
                                Delete
                              </Button>
                            </Grid>
                          </Grid>
                        </ExpansionPanelSummary>

                        <ExpansionPanelDetails className={classes.panelContent}>
                          <Grid container>
                            <Grid
                              item
                              xs={12}
                              className={classes.wordWrap}
                              style={{ marginBottom: "0" }}
                            >
                              {currentRequest.title}
                            </Grid>
                            <Grid
                              item
                              xs={12}
                              className={classes.wordWrap}
                              style={{ whiteSpace: "pre-line" }}
                            >
                              {currentRequest.desc}
                            </Grid>
                          </Grid>
                        </ExpansionPanelDetails>
                      </ExpansionPanel>
                    )
                )}
              </div>
            </div>
          ) : (
            "No requests found"
          ))}
      </div>
    );
  }
}

RequestsList.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStatesToProps = (states) => {
  return {
    user: states.user,
    requests: states.requests,
  };
};

export default connect(mapStatesToProps)(withStyles(styles)(RequestsList));
