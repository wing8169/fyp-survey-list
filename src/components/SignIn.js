import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    width: "400px",
    textAlign: "center",
    paddingTop: "50px",
    paddingBottom: "50px",
    overflow: "auto",
  },
  marginTopBottom: {
    marginTop: "20px",
    marginBottom: "20px",
  },
  button: {
    marginTop: "40px",
    marginBottom: "40px",
  },
}));

const SignIn = (props) => {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const signInWithEmailAndPasswordHandler = (event, email, password) => {
    event.preventDefault();
    props.auth
      .signInWithEmailAndPassword(email, password)
      .then((user) => {
        alert("Logged in!");
        props.handleCloseLogin();
      })
      .catch((error) => {
        console.log(error);
        alert("Failed to log in, invalid email or password.");
      });
  };

  const onChangeHandler = (event) => {
    const { name, value } = event.currentTarget;

    if (name === "userEmail") {
      setEmail(value);
    } else if (name === "userPassword") {
      setPassword(value);
    }
  };

  return (
    <div className={classes.paper}>
      <h2>Admin Sign In</h2>
      <div>
        <form>
          <div className={classes.marginTopBottom}>
            <TextField
              label="Email"
              type="email"
              name="userEmail"
              value={email}
              id="userEmail"
              onChange={(event) => onChangeHandler(event)}
            />
          </div>
          <div className={classes.marginTopBottom}>
            <TextField
              label="Password"
              type="password"
              name="userPassword"
              value={password}
              id="userPassword"
              onChange={(event) => onChangeHandler(event)}
            />
          </div>
          <div className={classes.button}>
            <Button
              variant="contained"
              color="primary"
              onClick={(event) => {
                signInWithEmailAndPasswordHandler(event, email, password);
              }}
            >
              Sign in
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default SignIn;
