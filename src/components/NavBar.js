import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import SignIn from "./SignIn";
import CreateRequest from "./CreateRequest";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  nav: {
    backgroundColor: "#9948f0",
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  modal: {
    margin: "auto",
    display: "flex",
    justifyContent: "center",
    height: "500px",
    overflowY: "scroll",
  },
  marginAll: {
    marginLeft: "10px",
    marginRight: "10px",
    marginTop: "10px",
    marginBottom: "10px",
  },
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
}));

const NavBar = (props) => {
  const classes = useStyles();
  const user = useSelector((state) => state.user);
  const [openLogin, setOpenLogin] = React.useState(false);
  const [openRequest, setOpenRequest] = React.useState(false);
  const [openMenu, setOpenMenu] = React.useState(false);

  const handleOpenLogin = () => {
    setOpenLogin(true);
  };

  const handleCloseLogin = () => {
    setOpenLogin(false);
  };

  const handleOpenRequest = () => {
    setOpenRequest(true);
  };

  const handleCloseRequest = () => {
    setOpenRequest(false);
  };

  const toggleDrawer = (event, value) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setOpenMenu(value);
  };

  const handleLogout = () => {
    props.auth
      .signOut()
      .then(() => {
        alert("signed out!");
      })
      .catch((error) => {
        console.log(error);
        alert("unable to sign out.");
      });
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appbar}>
        <Toolbar className={classes.nav}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={(event) => toggleDrawer(event, true)}
            edge="start"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            FYP Surveys Targeted For FCSIT Students Only
          </Typography>
          <Button
            variant="contained"
            color="primary"
            onClick={handleOpenRequest}
            className={classes.marginAll}
          >
            Request to include your FYP survey
          </Button>
          {user === null && (
            <Button
              variant="contained"
              color="primary"
              onClick={handleOpenLogin}
              className={classes.marginAll}
            >
              Login
            </Button>
          )}
          {user !== null && (
            <Button
              variant="contained"
              color="secondary"
              onClick={handleLogout}
              className={classes.marginAll}
            >
              Logout
            </Button>
          )}
        </Toolbar>
      </AppBar>
      {/* side navigation bar */}
      <Drawer open={openMenu} onClose={(event) => toggleDrawer(event, false)}>
        <div
          className={classes.list}
          role="presentation"
          onClick={(event) => toggleDrawer(event, false)}
          onKeyDown={(event) => toggleDrawer(event, false)}
        >
          <List>
            <Link to="/" style={{ textDecoration: "none", color: "black" }}>
              <ListItem button key="Home">
                <ListItemText primary="Home" />
              </ListItem>
            </Link>
            {user !== null && (
              <Link
                to="/requests"
                style={{ textDecoration: "none", color: "black" }}
              >
                <ListItem button key="Requests">
                  <ListItemText primary="Requests" />
                </ListItem>
              </Link>
            )}
          </List>
        </div>
      </Drawer>
      {/* login modal */}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={openLogin}
        onClose={handleCloseLogin}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openLogin}>
          <SignIn auth={props.auth} handleCloseLogin={handleCloseLogin} />
        </Fade>
      </Modal>
      {/* request modal */}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={openRequest}
        onClose={handleCloseRequest}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openRequest}>
          <CreateRequest
            database={props.database}
            handleCloseRequest={handleCloseRequest}
          />
        </Fade>
      </Modal>
    </div>
  );
};

export default NavBar;
