import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    width: "400px",
    paddingLeft: "50px",
    paddingTop: "50px",
    paddingBottom: "50px",
    overflow: "auto",
  },
  marginTopBottom: {
    marginTop: "20px",
    marginBottom: "20px",
  },
  button: {
    marginTop: "40px",
    marginBottom: "40px",
  },
}));

const CreateRequest = (props) => {
  const classes = useStyles();
  const [title, setTitle] = useState("");
  const [link, setLink] = useState("");
  const [desc, setDesc] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [confirmed, setConfirmed] = useState(false);
  const database = props.database;

  const createRequestHandler = (
    event,
    title,
    link,
    desc,
    name,
    email,
    confirmed
  ) => {
    event.preventDefault();
    if (!confirmed) {
      alert("please read and check the concent checkbox.");
    } else {
      // validation
      if (
        title === "" ||
        link === "" ||
        desc === "" ||
        name === "" ||
        email === ""
      ) {
        alert("invalid input.");
        return;
      }
      const id = title.replace(/[^0-9a-z]/gi, "").toLowerCase();
      database.ref("requests/" + id).set({
        title: title,
        link: link,
        desc: desc,
        name: name,
        email: email,
        isCreated: false,
      });
      alert("request sent!");
      props.handleCloseRequest();
    }
  };

  const onChangeHandler = (event) => {
    const { name, value, checked } = event.currentTarget;
    if (name === "title") {
      setTitle(value);
    } else if (name === "link") {
      setLink(value);
    } else if (name === "desc") {
      setDesc(value);
    } else if (name === "name") {
      setName(value);
    } else if (name === "email") {
      setEmail(value);
    } else if (name === "confirmed") {
      setConfirmed(checked);
    }
  };

  return (
    <div className={classes.paper}>
      <h2>Request to Include FYP Form</h2>
      <div>
        <form>
          <div className={classes.marginTopBottom}>
            <TextField
              label="Survey Title"
              type="text"
              name="title"
              value={title}
              id="title"
              onChange={(event) => onChangeHandler(event)}
              required
            />
          </div>
          <div className={classes.marginTopBottom}>
            <TextField
              label="Google Form Link"
              type="text"
              name="link"
              value={link}
              id="link"
              onChange={(event) => onChangeHandler(event)}
              required
            />
          </div>
          <div className={classes.marginTopBottom}>
            <TextField
              label="Description"
              type="text"
              name="desc"
              value={desc}
              id="desc"
              multiline
              rows={4}
              variant="outlined"
              onChange={(event) => onChangeHandler(event)}
              required
            />
          </div>
          <div className={classes.marginTopBottom}>
            <TextField
              label="Name"
              type="text"
              name="name"
              value={name}
              id="name"
              onChange={(event) => onChangeHandler(event)}
              required
            />
          </div>
          <div className={classes.marginTopBottom}>
            <TextField
              label="Email to Contact"
              type="email"
              name="email"
              value={email}
              id="email"
              onChange={(event) => onChangeHandler(event)}
              required
            />
          </div>
          <FormControlLabel
            control={
              <Checkbox
                checked={confirmed}
                onChange={(event) => onChangeHandler(event)}
                name="confirmed"
                id="confirmed"
              />
            }
            label="I hereby understand that this website is only targeting to FCSIT students."
          />
          <div className={classes.button}>
            <Button
              variant="contained"
              color="primary"
              onClick={(event) => {
                createRequestHandler(
                  event,
                  title,
                  link,
                  desc,
                  name,
                  email,
                  confirmed
                );
              }}
            >
              Send Request
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default CreateRequest;
