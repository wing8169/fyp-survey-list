const surveysReducer = (state = [], action) => {
  switch (action.type) {
    case "SET_SURVEYS":
      return action.payload;
    default:
      return state;
  }
};

export default surveysReducer;
