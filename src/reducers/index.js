import surveysReducer from "./surveys";
import userReducer from "./user";
import requestsReducer from "./requests";
import { combineReducers } from "redux";

const allReducers = combineReducers({
  surveys: surveysReducer,
  user: userReducer,
  requests: requestsReducer,
});

export default allReducers;
