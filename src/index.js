import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { createStore } from "redux";
import allReducer from "./reducers/";
import { Provider } from "react-redux";
import { database, auth } from "./firebase";

let store = createStore(
  allReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

// survey listener
database.ref("surveys").on("value", function (snapshot) {
  let data = snapshot.val();
  let arr = [];
  let i = 0;
  for (var d in data) {
    data[d].key = i;
    arr.push(data[d]);
    i++;
  }
  // sort data
  arr.sort(function (a, b) {
    return a.count - b.count;
  });
  // dispatch update local surveys
  store.dispatch({ type: "SET_SURVEYS", payload: arr });
});

// request listener
database.ref("requests").on("value", function (snapshot) {
  let data = snapshot.val();
  let arr = [];
  let i = 0;
  for (var d in data) {
    data[d].key = i;
    arr.push(data[d]);
    i++;
  }
  arr.sort(function (a, b) {
    return a.isCreated - b.isCreated;
  });
  store.dispatch({ type: "SET_REQUESTS", payload: arr });
});

// auth listener
auth.onAuthStateChanged((userAuth) => {
  // dispatch update local user
  store.dispatch({ type: "SET_USER", payload: userAuth });
});

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
